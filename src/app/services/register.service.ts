import { Injectable } from '@angular/core';
import { Observable, of} from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  constructor() {}

  submitRegister(): Observable<any> {
    return of(true).pipe(delay(4000));
  }
}

import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { RegisterService } from '../../services/register.service';
import * as moment from 'moment';

interface List {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.less'],
})
export class RegisterFormComponent implements OnInit, AfterViewInit {
  @ViewChild('completeSwal') private completeDialogRef: SwalComponent;
  isLoading = false;
  formGroupName: FormGroup;
  titleList: List[] = [
    { value: 'mr', viewValue: 'นาย' },
    { value: 'mrs', viewValue: 'นาง' },
    { value: 'miss', viewValue: 'นางสาว' },
    { value: 'other', viewValue: 'อื่นๆ' },
  ];

  yearList: Array<number> = this.getYearList();
  monthList: Array<string> = [
    'มกราคม',
    'กุมภาพันธ์',
    'มีนาคม',
    'เมษายน',
    'พฤษภาคม',
    'มิถุนายน',
    'กรกฏาคม',
    'สิงหาคม',
    'กันยายน',
    'คุลาคม',
    'พฤศจิกายน',
    'ธันวาคม',
  ];
  dayList = this.getDayList();

  registerForm: FormGroup;

  constructor(
    public registerService: RegisterService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      name: ['', [Validators.required, Validators.pattern('[ก-๛]*')]],
      surname: ['', [Validators.required, Validators.pattern('[ก-๛]*')]],
      id: ['', [Validators.required, Validators.minLength(13)]],
      backId: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      year: ['', [Validators.required]],
      month: ['', [Validators.required]],
      day: ['', [Validators.required]],
    });

    this.registerForm.valueChanges.subscribe(() => this.onFormValuesChanged());
  }

  ngAfterViewInit(): void {}

  onFormValuesChanged(): void {
    for (const field in this.registerForm) {
      if (!this.registerForm.hasOwnProperty(field)) {
        continue;
      }
      // Clear previous errors
      this.registerForm[field] = {};
      // Get the control
      const control = this.registerForm.get(field);
      if (control && control.dirty && !control.valid) {
        this.registerForm[field] = control.errors;
      }
    }
  }

  handleSubmit(): void {
    this.isLoading = true;
    if (this.registerForm.invalid) {
      Object.keys(this.registerForm.controls).forEach((field) => {
        const control = this.registerForm.get(field);
        control.markAsTouched({ onlySelf: true });
      });
      this.isLoading = false;
    } else {
      this.registerService.submitRegister().subscribe((result) => {
        console.log('result', result);
        this.isLoading = false;
        this.completeDialogRef.fire();
      });
    }
  }

  private getYearList(): Array<number> {
    const yearList = [];
    const currentYear = moment().year();

    for (let count = currentYear - 50; count <= currentYear + 50; count++) {
      yearList.push(count);
    }
    return yearList;
  }

  private getDayList(): Array<string> {
    const dayList = [];
    const daysInMonth = moment().daysInMonth();

    for (let count = 1; count <= daysInMonth; count++) {
      dayList.push(count);
    }
    return dayList;
  }
}
